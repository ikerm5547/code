/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practica03;

/**
 *
 * @author Iker Martinez
 */
public class Registro {
    private int codigoProducto;
    private String descripcion;
    private String unidadMedida;
    private float PrecioC;
    private float PrecioV;
    private int cantidad;
    
    public Registro(){
     this.codigoProducto=1203;
     this.descripcion="Atun de agua";
     this.unidadMedida="pieza";
     this.PrecioC=10.00f;
     this.PrecioV=15.00f;
     this.cantidad=100;
    }
    public Registro(int codigoProducto, String descripcion, String unidadMedida, float precioC, float precioV, int cantidad ){
     this.codigoProducto = codigoProducto;
     this.descripcion = descripcion;
     this.unidadMedida = unidadMedida;
     this.PrecioC = precioC;
     this.PrecioV = precioV;
     this.cantidad = cantidad;
    }
    public Registro(Registro otro){
    this.codigoProducto = otro.codigoProducto;
     this.descripcion = otro.descripcion;
     this.unidadMedida = otro.unidadMedida;
     this.PrecioC = otro.PrecioC;
     this.PrecioV = otro.PrecioV;
     this.cantidad = otro.cantidad;
    }

    public int getCodigoProducto() {
        return codigoProducto;
    }

    public void setCodigoProducto(int codigoProducto) {
        this.codigoProducto = codigoProducto;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getUnidadMedida() {
        return unidadMedida;
    }

    public void setUnidadMedida(String unidadMedida) {
        this.unidadMedida = unidadMedida;
    }

    public float getPrecioC() {
        return PrecioC;
    }

    public void setPrecioC(float PrecioC) {
        this.PrecioC = PrecioC;
    }

    public float getPrecioV() {
        return PrecioV;
    }

    public void setPrecioV(float PrecioV) {
        this.PrecioV = PrecioV;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }
    
    public float calcularPrecioVenta(){
    float venta = 0;
    venta = this.PrecioV * this.cantidad;
    return venta;
    }
    public float calcularPrecioCompra(){
    float compra = 0;
    compra = this.PrecioC * this.cantidad;
    return compra;
    }
    public float calcularGanancia(){
    float ganancia =0;
    float venta = calcularPrecioVenta();
    float compra = calcularPrecioCompra();
    ganancia = venta - compra; 
    return ganancia;        
    }

    String getdescripcion() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
